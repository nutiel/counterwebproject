package com.qaagility.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AboutTest {
    
    @Test
    public void runDescTest() throws Exception {
        String s = new About().desc();
        String test = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
        assertEquals("desc", test, s);
    }
}
